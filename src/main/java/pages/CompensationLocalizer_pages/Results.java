package pages.CompensationLocalizer_pages;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import static driverfactory.Driver.*;
import static pages.CompensationLocalizer_pages.Input.*;
import static utilities.InitTests.dir_path;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Results {
@FindBy(xpath = "//th[text()='Total Compensation' and contains(@class,'col01')]//following::td[1]")
WebElement HomeTotalCompensation;
@FindBy(xpath = "//th[text()='Net Income'][1]//following::td[1]")
WebElement HomeNetIncome;
@FindBy(xpath = "//th[text()='Adjusted Net Income' and contains(@class,'col01')]//following::td[1]")
WebElement HomeAdjustedNetIncome;
@FindBy(xpath = "//th[text()='Calculated Base Pay' and contains(@class,'col02')]//following::td[1]")
WebElement HostCalculatedBasePay;
@FindBy(xpath = "//td[text()='Total Income Tax' and contains(@class,'col02')]//following::td[1]")
WebElement HostPersonalIncomeTax;
@FindBy(xpath = "//td[text()='Social Security' and contains(@class,'col02')]//following::td[1]")
WebElement HostSocialSecurity;
@FindBy(xpath = "//td[text()='Family Allowance' and contains(@class,'col02')]//following::td[1]")
WebElement HostFamilyAllowance;
@FindBy(xpath = "//td[text()='Housing' and contains(@class,'col02')]//following::td[1]")
WebElement HostHousing;
@FindBy(xpath = "//th[text()='Total Compensation' and contains(@class,'col02')]//following::td[1]")
WebElement HostTotalCompensation;
@FindBy(xpath = "//th[text()='Net Income'][2]//following::td[1]")
WebElement HostNetIncome;
@FindBy(xpath = "//th[contains(text(),'Adjusted Net Income') and contains(@class,'col02')]//following::td[1]")
WebElement HostAdjustedNetIncome;
@FindBy(xpath = "//th[contains(text(),'Proposed Base Pay')]//following::td[1]")
WebElement ProposedBasePay;
@FindBy(xpath = "//td[text()='Hardship Premium' and contains(@class,'col03')]//following::td[1]")
WebElement HardshipPremium;
@FindBy(xpath = "//td[text()='Total Income Tax' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostPersonalIncomeTax;
@FindBy(xpath = "//td[text()='Social Security' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostSocialSecurity;
@FindBy(xpath = "//td[text()='Housing' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostHousing;
@FindBy(xpath = "//td[text()='Family Allowance' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostFamilyAllowance;
@FindBy(xpath = "//th[text()='Total Compensation' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostTotalCompensation;
@FindBy(xpath = "//th[text()='Net Income'][3]//following::td[1]")
WebElement ProposedHostNetIncome;
@FindBy(xpath = "//th[text()='Adjusted Net Income' and contains(@class,'col03')]//following::td[1]")
WebElement ProposedHostAdjustedNetIncome;
@FindBy(xpath="//button[text()='Run Another Calculation']")
WebElement RunAnotherCalcButton;
@FindBy(xpath = "//button[contains(text(),'Export')]")
WebElement ExportButton;
@FindBy(xpath = "//a[contains(text(),'EXCEL')]")
public static WebElement ExcelOption;
@FindBy(xpath = "//a[contains(text(),'PDF')]")
public static WebElement PDFOption;

public Results() {
	PageFactory.initElements(driver, this);
}

public int computeHomeCompensation() {
	HomeTotalCompensationValue = Integer.parseInt(HomeTotalCompensation.getText());
	int amount = AnnualBasePayValue+Allowance1+Allowance2-Deduction1-Deduction2;
	return amount;
}
public int computeHomeNetIncome() {
	HomeNetIncomeValue = Integer.parseInt(HomeNetIncome.getText());
	int amount = HomeTotalCompensationValue-PersonalIncomeTaxValue-SocialSecurityValue+FamilyAllowanceValue;
	return amount;
}
public int computeHomeAdjustedNetIncome() {
	HomeAdjustedNetValue = Integer.parseInt(HomeAdjustedNetIncome.getText());
	int amount = HomeNetIncomeValue-HousingValue+COLAValue;
	return amount;
}
public int computeCalculatedHostCompensation() {
	HostTotalCompensationValue = Integer.parseInt(HostTotalCompensation.getText());
	HostCalculatedBasePayValue = Integer.parseInt(HostCalculatedBasePay.getText()); 
	int amount = HostCalculatedBasePayValue + AdditionalTaxableIncomeValue;
	return amount;
}
public int computeHostNetIncome() {
	HostNetIncomeValue = Integer.parseInt(HostNetIncome.getText());
	HostPersonalIncomeTaxValue = Integer.parseInt(HostPersonalIncomeTax.getText().replaceAll("[\\(\\)]", ""));
	HostSocialSecurityValue = Integer.parseInt(HostSocialSecurity.getText().replaceAll("[\\(\\)]", ""));
	HostFamilyAllowanceValue = Integer.parseInt(HostFamilyAllowance.getText());
	int amount = HostTotalCompensationValue-HostPersonalIncomeTaxValue-HostSocialSecurityValue+HostFamilyAllowanceValue;
	return amount;
}
public int computeHostAdjustedNetIncome() {
	HostAdjustedNetValue = Integer.parseInt(HostAdjustedNetIncome.getText());
	HostHousingValue = Integer.parseInt(HostHousing.getText().replaceAll("[\\(\\)]", ""));
	int amount = HostNetIncomeValue-HostHousingValue;
	return amount;
}
public boolean computeAdjustedNetIncomeInHostDifference(String checkSign, int value) {
	 int amount = (int)Math.round((HomeNetIncomeValue-HousingValue+COLAValue)*ExchangeRateValue);
	 switch(checkSign) {
	 case "LessThan":
		 if((amount-HostAdjustedNetValue)<value)
		 return true;
	 case "GreaterThan":
		 if((amount-HostAdjustedNetValue)>value)
			 return true;
	 case "Equals":
		 if((amount-HostAdjustedNetValue)==value)
			 return true;
	 }
	  
		 return false;
}
public int computeProposedHostCompensation() {
	ProposedHostTotalCompensationValue = Integer.parseInt(ProposedHostTotalCompensation.getText());
	if(ProposedHostBasePayValue==0) {
	ProposedHostBasePayValue = Integer.parseInt(ProposedBasePay.getText());
	HardshipPremiumValue = Integer.parseInt(HardshipPremium.getText());
	}
	int amount = ProposedHostBasePayValue+HardshipPremiumValue+EducationCostValue+CustomAllowanceValue;
	return amount;
}
public int computeProposedHostNetIncome() {
	ProposedHostNetIncomeValue = Integer.parseInt(ProposedHostNetIncome.getText());
	ProposedHostPersonalIncomeTaxValue = Integer.parseInt(ProposedHostPersonalIncomeTax.getText().replaceAll("[\\(\\)]", ""));
	ProposedHostSocialSecurityValue = Integer.parseInt(ProposedHostSocialSecurity.getText().replaceAll("[\\(\\)]", ""));
	ProposedHostFamilyAllowanceValue = Integer.parseInt(ProposedHostFamilyAllowance.getText());
	int amount = ProposedHostTotalCompensationValue-ProposedHostPersonalIncomeTaxValue-ProposedHostSocialSecurityValue+ProposedHostFamilyAllowanceValue;
	return amount; 
}
public int computeProposedHostAdjustedNetIncome() {
	ProposedHostAdjustedNetValue = Integer.parseInt(ProposedHostAdjustedNetIncome.getText());
	ProposedHostHousingValue = Integer.parseInt(ProposedHostHousing.getText().replaceAll("[\\(\\)]", ""));
	int amount = ProposedHostNetIncomeValue-ProposedHostHousingValue;
	return amount;
}
public boolean computeCompensationDifference(String checkSign, int value) {
	switch(checkSign) {
	 case "LessThan":
		 if((ProposedHostTotalCompensationValue - HostTotalCompensationValue)<value)
		 return true;
	 case "GreaterThan":
		 if((ProposedHostTotalCompensationValue - HostTotalCompensationValue)>value)
			 return true;
	 case "Equal":
		 if((ProposedHostTotalCompensationValue - HostTotalCompensationValue)==value)
			 return true;
	 }
	  return false;
		 
}public boolean isFileDownloaded(String downloadPath, String fileName) {
	  File dir = new File(downloadPath);
	  File[] dirContents = dir.listFiles();
	  System.out.println("Inside download check");
	  for (int i = 0; i < dirContents.length; i++) {
	      if (dirContents[i].getName().contains(fileName)) {
	          System.out.println("File has been found, it can now be deleted:"); 
	          dirContents[i].delete();
	          return true;
	      }
	          }
	      return false;
	  } 
public void excelExport() throws FindFailed, IOException {
	clickElement(ExportButton);
//	clickElement(ExcelOption);	 
	//popUpHandle();
	
}
public void pdfExport() throws FindFailed, IOException {
	clickElement(ExportButton);
//	clickElement(PDFOption);
	//popUpHandle();
	
}
public void runAnotherCalculation() {
	clickElement(RunAnotherCalcButton);
}
public void popUpHandle() throws FindFailed {
	Screen screen = new Screen();
	Pattern save = new Pattern(dir_path + "/src/main/resources/WindowScreens/SaveButtonPopUp.png");
	screen.wait(save, 10);
	screen.doubleClick(save);
}
}
