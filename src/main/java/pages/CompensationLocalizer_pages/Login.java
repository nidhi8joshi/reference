package pages.CompensationLocalizer_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.implicitWait;
import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.CompensationLocalizer_pages.MFA;

public class Login {
	@FindBy(xpath = "//span[contains(text(),'My Account')]")
	WebElement MyAccountButton;
	@FindBy(xpath = "//span[contains(text(),'Login')]")
	WebElement LoginButton;
	@FindBy(id = "ctl00_MainSectionContent_Email")
	WebElement EmailID;
	@FindBy(id = "ctl00_MainSectionContent_Password")
	WebElement Password;
	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	WebElement SignInButton;
	@FindBy (xpath = "//*[contains(text(),'Select a customer:')]")
	public WebElement AccountSelectionHeader;
	public WebElement Client; 
	public WebElement ClientAccount;
	@FindBy(xpath = "//label[contains(text(),'You are signed in as:')]/following::a[1]")
	public WebElement LoggedInAccount;
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement ContinueButton;
	

	public Login() {
		PageFactory.initElements(driver, this);
		implicitWait(driver,10);
	}

	public void login(String username, String password) throws InterruptedException {
		clickElement(MyAccountButton);
		clickElement(LoginButton);
		setInput(EmailID, username);
		setInput(Password, password);
		clickElement(SignInButton);
		while (driver.getPageSource().contains("Contact Us for Help")) {
			navigateBack(driver);
			setInput(Password, password + Keys.ENTER);
			
		}
		MFA mfa = new MFA();
		mfa.authenticate();
	}
	public void accountSelection(String CustomerName, String AccountName) {
		Client=driver.findElement(By.partialLinkText(CustomerName));
		clickElement(Client);
		ClientAccount=driver.findElement(By.partialLinkText(AccountName));
		clickElement(ClientAccount);
		clickElement(ContinueButton);
		delay(2000);
	}
		
}
