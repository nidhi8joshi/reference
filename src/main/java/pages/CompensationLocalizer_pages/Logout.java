package pages.CompensationLocalizer_pages;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Logout {

	@FindBy(xpath = "//a[text()='Return to Mobility Exchange']")
	WebElement ReturnToMobilityExchange;
	@FindBy(xpath = "//div[contains(@class,'visible-lg')]//child::span[contains(text(),'My Account')]")
	WebElement MyAccountButton;
	@FindBy(linkText = "Logout")
	WebElement LogoutButton;
	@FindBy(linkText = "Are you an expatriate? Please take a short survey")
	public WebElement HomePageSurvey;
	
	
	public Logout() {
		PageFactory.initElements(driver, this);
	}
	
	public void logout() {
		clickElement(ReturnToMobilityExchange);
		driver.manage().window().maximize();
		clickElement(MyAccountButton);
		clickElement(LogoutButton);
		waitForElementToDisplay(HomePageSurvey);
	}
}
