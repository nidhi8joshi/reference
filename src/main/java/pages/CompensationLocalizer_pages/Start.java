package pages.CompensationLocalizer_pages;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Start {

	@FindBy (xpath = "//*[@class='app-title']")
	public	WebElement ApplicationName;
	@FindBy(xpath ="//li[@class='active']//following::div")
	public WebElement ActiveTab;
	@FindBy (xpath = "//li[@id='emailId']//a")
	WebElement AccountDetailsDropdown;
	@FindBy(xpath = "//div[contains(text(),'ACCOUNT')]//following::div[1]")
	public WebElement LoggedInAccount;
	@FindBy(xpath = "//*[contains(text(),'Start Calculation')]")
	public WebElement StartCalculationHeader;
	
	@FindBy(xpath = "//select[@id='HomeLocation']")
//	@FindBy(xpath = "//select[@id='HomeLocationGrey']")
	WebElement HomeLocationDropdown;//Canada, Vancouver
	
	@FindBy(xpath = "//select[@id='HostLocation']")
//	@FindBy(xpath = "//select[@id='HostLocationGrey']")
	WebElement HostLocationDropdown;//Switzerland, Zurich
	
	@FindBy(id = "checkboxId")
	WebElement AcknowledgementCheckBox;
	@FindBy (xpath = "//button[@id='submitbtnId']")
	WebElement ContinueButton;
	
	public Start() {
		PageFactory.initElements(driver, this)	;
		}
		public void openCompensationLocalizer() {
			clickElement(driver.findElement(By.partialLinkText("Compensation Localizer")));
			switchToWindow("COMPENSATOIN LOCALIZER");
			driver.manage().window().maximize();
			clickElement(AccountDetailsDropdown);
		}
		public void startCalculation(String home, String host) {
			selEleByVisbleText(HomeLocationDropdown,home);
			selEleByVisbleText(HostLocationDropdown,host);
			clickElement(AcknowledgementCheckBox);
			clickElement(ContinueButton);
			//delay(7000);
		}
}
