package pages.CompensationLocalizer_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;
import static pages.CompensationLocalizer_pages.Input.*;

public class Preview {
	@FindBy(xpath = "//label[text()='Allowance1' and @data-bind]//following::label[1]")
	public WebElement Allowance1Preview;
	@FindBy(xpath = "//label[text()='Allowance2' and @data-bind]//following::label[1]")
	public WebElement Allowance2Preview;
	@FindBy(xpath = "//label[text()='Deduction1' and @data-bind]//following::label[1]")
	public WebElement Deduction1Preview;
	@FindBy(xpath = "//label[text()='Deduction2' and @data-bind]//following::label[1]")
	public WebElement Deduction2Preview;
	@FindBy (xpath = "//input[contains(@name,'SocialSecurity')]")
	WebElement SocialSecurity;
	@FindBy (xpath = "//input[contains(@name,'PersonalIncomeTax')]")
	WebElement PersonalIncomeTax;
	@FindBy (xpath = "//input[contains(@name,'FamilyAllowance')]")
	WebElement FamilyAllowance;
	@FindBy(xpath = "//label[contains(@data-bind,'.NetSalary')]")
	WebElement NetIncome;
	@FindBy(xpath = "//input[contains(@name,'Housing')]")
	WebElement Housing;
	@FindBy(xpath = "//input[contains(@name,'SpendableIncome')]")
	WebElement SpendableIncome;
	@FindBy(xpath = "//input[contains(@name,'COLI')]")
	WebElement COLI;
	@FindBy(xpath = "//label[contains(@name,'COLA')]")
	WebElement COLA;
	@FindBy(xpath = "//label[contains(@data-bind,'HomeOption.AdjustedNetSalary')]")
	WebElement AdjustedNetIncome;
	@FindBy(xpath = "//label[contains(@data-bind,'ExchangeRate')]")
	WebElement ExchangeRate;//substring(29,36)
	@FindBy(xpath = "//label[contains(@data-bind,'HostOption.AdjustedNetSalary')]")
	WebElement AdjustedNetIncomeInHost;
	@FindBy(xpath = "//input[contains(@data-bind,'checked: statusEducationAllowance')]")
	WebElement EducationAllowanceCheckBox;
	@FindBy(xpath = "//input[contains(@value,'Education Costs')]")
	WebElement EducationCostsButton;
	@FindBy(id = "btnPrimaryFee")
	WebElement AddPrimaryFeeButton;
	@FindBy(id = "btnSubmitEducationCost")
	WebElement SubmitEducationCostButton;
	@FindBy(id = "EducationCost")
	WebElement EducationCostInPopUp;
	@FindBy(id = "txtEducationAllownce")
	public WebElement EducationCostAdded;
	@FindBy(xpath = "//input[contains(@data-bind,'checked: statusHardshipPremium')]")
	WebElement HardshipPremiumCheckBox;
	@FindBy(id = "txtHardship")
	public WebElement HardshipPercentageOfGross;
	@FindBy(id = "inputCheckbox")
	WebElement AcknowledgementCheckBox;
	@FindBy(xpath = "//input[@name='HardshipPremium']")
	WebElement HardshipPremium;
	@FindBy(xpath = "//span[text()='%']")
	WebElement PercentageSymbol;
	@FindBy(id = "cbAllowancechk1")
	WebElement CustomLabelCheckBox;
	@FindBy(id = "inputAllowance1")
	WebElement CustomLabelInput;
	@FindBy(xpath = "//input[@name='inputallowancecurrency1']")
	WebElement CustomLabelValue;
	@FindBy(id="btnContinue")
	WebElement ContinueButton;
	@FindBy(xpath = "//img[@class='loadingImage']")
	WebElement BufferImage;
	
	public Preview() {
		PageFactory.initElements(driver, this);
	}
	public boolean checkFamilyAllowance(String checkSign,int value) {
		FamilyAllowanceValue = Integer.parseInt(FamilyAllowance.getAttribute("value"));
		 switch(checkSign) {
		 case "LessThan":
			 if(FamilyAllowanceValue<value)
			 return true;
		 case "GreaterThan":
			 if(FamilyAllowanceValue>value)
				 return true;
		 case "Equals":
			 if(FamilyAllowanceValue==value)
				 return true;
		 }
		return false;
	}
	public int computeNetIncome() {
		NetIncomeValue = Integer.parseInt(NetIncome.getText());
		SocialSecurityValue = Integer.parseInt(SocialSecurity.getAttribute("value"));
		PersonalIncomeTaxValue= Integer.parseInt(PersonalIncomeTax.getAttribute("value"));
		int amount = AnnualBasePayValue+Allowance1+Allowance2-Deduction1-Deduction2-SocialSecurityValue-PersonalIncomeTaxValue+FamilyAllowanceValue;
		return amount;
	}
	public int computeCostOfLivingAdjustments() {
		COLAValue = Integer.parseInt(COLA.getText());
		COLIValue = Integer.parseInt(COLI.getAttribute("value"));
		SpendableIncomeValue = Integer.parseInt(SpendableIncome.getAttribute("value"));
		int amount = (int)Math.round(((COLIValue-100)*SpendableIncomeValue)/100.0);
		return amount;
	}
	public boolean checkIfCOLAIsNegative() {
		if(COLAValue<0)
			return true;
		else
			return false;
	}
	public int computeAdjustedNetIncome() {
		AdjustedNetIncomeValue = Integer.parseInt(AdjustedNetIncome.getText());
		HousingValue= Integer.parseInt(Housing.getAttribute("value"));
		int amount = NetIncomeValue-HousingValue+COLAValue;
		return amount;
	}
	public int computeAdjustedNetIncomeInHost() {
		AdjustedNetIncomeInHostValue = Integer.parseInt(AdjustedNetIncomeInHost.getText());
		ExchangeRateValue = Double.parseDouble(ExchangeRate.getText().substring(29));
		 int amount = (int) Math.round(AdjustedNetIncomeValue * ExchangeRateValue);
		 return amount;
	}
	public void addEducationAllowance() {
		clickElement(EducationAllowanceCheckBox);
		clickElement(EducationCostsButton);
		delay(5000);
		switchToWindowWithURL("EducationCost");
		clickElement(AddPrimaryFeeButton);
		clickElement(SubmitEducationCostButton);
		EducationCostValue = Integer.parseInt(EducationCostInPopUp.getAttribute("value"));
		switchToWindow("COMPENSATION LOCALIZER");
	}
	public void addHardshipPremium() {
		clickElement(HardshipPremiumCheckBox);
	}
	public int computeHardshipPremium(String percentage) {
		setInput(HardshipPercentageOfGross,percentage);
		clickElement(PercentageSymbol);
		HardshipPremiumValue = Integer.parseInt(HardshipPremium.getAttribute("value"));
		int intpercentage = Integer.parseInt(percentage);
		int amount = (ProposedHostBasePayValue*intpercentage)/100;
		return amount;
	}
	public void setCustomAllowance(String income) {
		clickElement(CustomLabelCheckBox);
		setInput(CustomLabelInput,"Custom1");
		setInput(CustomLabelValue,income);
		CustomAllowanceValue = Integer.parseInt(income);
	}
	public void submit() {
		clickElement(AcknowledgementCheckBox);
		clickElement(ContinueButton);
		delay(10000);
	}
}
