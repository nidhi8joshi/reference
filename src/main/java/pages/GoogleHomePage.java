package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class GoogleHomePage {
	@FindBy(name="q")
	public  WebElement gTextbox;
	@FindBy(name = "btnK")
	public  WebElement gSearchBtn;
	
	
	@FindBy(css = "h3.r a")
	public static   WebElement firstLnk;
	

	
	public WebElement getgTextbox() {
		return gTextbox;
	}
	public void setgTextbox(WebElement gTextbox) {
		this.gTextbox = gTextbox;
	}
	public WebElement getgSearchBtn() {
		return gSearchBtn;
	}
	public void setgSearchBtn(WebElement gSearchBtn) {
		this.gSearchBtn = gSearchBtn;
	}
	public WebElement getFirstLnk() {
		return firstLnk;
	}
	public void setFirstLnk(WebElement firstLnk) {
		this.firstLnk = firstLnk;
	}
	public GoogleHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
public void search(String searchkey) throws InterruptedException
{
	
	setInput(gTextbox,searchkey);
	
	clickElement(gSearchBtn);

}
}
