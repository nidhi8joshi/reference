package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class LoginPage {

	@FindBy(css = "button[class*='loginBtn']")
	public static WebElement loginBtn;
	@FindBy(id = "txtEmail")
	public static WebElement emailInput;
	
	@FindBy(css = "input[id='txtPassword-clone']")
	public static WebElement password;
	@FindBy(css = "input[id='txtPassword']")
	public static WebElement password1;
	
	@FindBy(css = "button[title='Enter']")
	public static WebElement enterBtn;
	
	@FindBy(id = "ContentPlaceHolder1_PassiveSignInButton")
	public static WebElement nextBtn;
	

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
public void login(String uname, String pwd) throws InterruptedException
{
	waitForElementToDisplay(loginBtn);
	clickElement(loginBtn);
	clickElement(nextBtn);

	waitForElementToDisplay(emailInput);
	setInput(emailInput,uname);
	waitForElementToDisplay(password);
	clickElement(password);
	waitForElementToDisplay(password1);
	setInput(password1,pwd);
	clickElement(enterBtn);

}
}
