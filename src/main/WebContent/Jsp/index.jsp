<!DOCTYPE html>
<%@page import="utilities.MyExtentReports"%>
<html lang="en">
<head>
<script type="text/javascript">
function selectAllReg(){
				var items=document.getElementsByName('RegTestCases');
				for(var i=0; i<items.length; i++){
					if(items[i].type=='checkbox')
						items[i].checked=true;
				}
			}
			
function selectAllSauce(){
	var items=document.getElementsByName('SaucelabTests');
	for(var i=0; i<items.length; i++){
		if(items[i].type=='checkbox')
			items[i].checked=true;
	}
}
function selectAllSmoke(){
	var items=document.getElementsByName('SmokeTestCases');
	for(var i=0; i<items.length; i++){
		if(items[i].type=='checkbox')
			items[i].checked=true;
	}
}

function selectAllRest(){
	var items=document.getElementsByName('RestAPITests');
	for(var i=0; i<items.length; i++){
		if(items[i].type=='checkbox')
			items[i].checked=true;
	}
}
function selectAllGrid(){
	var items=document.getElementsByName('GridTests');
	for(var i=0; i<items.length; i++){
		if(items[i].type=='checkbox')
			items[i].checked=true;
	}
}
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Creative One Page Parallax Template">
<meta name="keywords"
	content="Creative, Onepage, Parallax, HTML5, Bootstrap, Popular, custom, personal, portfolio" />
<meta name="author" content="">
<title>Mercer Test Automation Framework</title>
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/prettyPhoto.css" rel="stylesheet">
<link href="./css/font-awesome.min.css" rel="stylesheet">
<link href="./css/animate.css" rel="stylesheet">
<link href="./css/main.css" rel="stylesheet">
<link href="./css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]-->
<link rel="shortcut icon" href="Jsp/images/ico/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<style>
#LockOn {
	display: block;
	visibility: hidden;
	position: absolute;
	z-index: 999;
	top: 0px;
	left: 0px;
	width: 105%;
	height: 105%;
	background-color: white;
	vertical-align: bottom;
	padding-top: 20%;
	filter: alpha(opacity = 75);
	opacity: 0.75;
	font-size: large;
	color: blue;
	font-style: italic;
	font-weight: 400;
	background-image: url("images/loader.gif");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
}

html, body {
	max-width: 100%;
	overflow-x: hidden;
}
</style>
</head>
<!--/head-->
<body>
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner">
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
				<div class="ball"></div>
			</div>
		</div>
	</div>
	<!--/.preloader-->
	<div id="LockOn"></div>
	<!--/.Loading Image-->

	<header id="navigation">
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#navigation">
						<h1>
							<img src="./images/mercer.png" alt="logo">
						</h1>
					</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="scroll active"><a href="#navigation"><b>Home</b></a></li>
												<li class="scroll"><a href="#smoke"><b>Smoke</b></a></li>
						
						<li class="scroll"><a href="#regression"><b>Regression</b></a></li>
				
						<li class="scroll"><a href="#webServices"><b>Web Services</b></a></li>
						<li class="scroll"><a href="#saucelabs"><b>Sauce Labs</b></a></li>
						<li class="scroll"><a href="#seleniumgrid"><b>Selenium Grid</b></a></li>
						
						<li class="scroll"><a href="./ATU Reports/Results/ConsolidatedPage.html" target="_blank"><b>ATU Reports</b></a></li>
							<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.DateUtils"%>

	<%
		String time=DateUtils.getCurrTimeStamp();
	 MyExtentReports.timeStamp=time;
	
	out.println("<li class='scroll'><a href='./extentReports/"+time+".html' target='_blank'><b>Extent Reports</b></a></li>");
	%>
<!-- 												<li class="scroll"><a href="#services"><b>Services</b></a></li>
 -->						
							 <li class="scroll"><a href="#uiVerification"><b>UI Verification</b></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!--/navbar-->
	</header>
	<!--/#navigation-->

	<section id="home">
		<div class="home-pattern"></div>
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#main-carousel" data-slide-to="1"></li>
				<li data-target="#main-carousel" data-slide-to="2"></li>
			</ol>
			<!--/.carousel-indicators-->
			<div class="carousel-inner">
				<div class="item active"
					style="background-image: url(./images/slider/slider1.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInDown">Live Testing</h2>
							<p class="animated bounceInUp">Manual and Automated Tests on
								100's of Real devices & Browsers</p>
							<a class="btn btn-default slider-btn animated fadeIn" href="#">Get
								Started</a>
						</div>
					</div>
				</div>
				<div class="item"
					style="background-image: url(./images/slider/slider4.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInDown">Visual
								Verification</h2>
							<p class="animated bounceInUp">Automated Screenshots &
								Comparision</p>
							<a class="btn btn-default slider-btn animated fadeIn" href="#">Get
								Started</a>
						</div>
					</div>
				</div>
				<div class="item"
					style="background-image: url(./images/slider/slider3.jpg)">
					<div class="carousel-caption">
						<div>
							<h2 class="heading animated bounceInRight">E2E Testing</h2>
							<p class="animated bounceInLeft">Covers all functional and
								Non-functional aspects</p>
							<a class="btn btn-default slider-btn animated bounceInUp"
								href="#">Get Started</a>
						</div>
					</div>
				</div>
			</div>
			<!--/.carousel-inner-->

			<a class="carousel-left member-carousel-control hidden-xs"
				href="#main-carousel" data-slide="prev"><i
				class="fa fa-angle-left"></i></a> <a
				class="carousel-right member-carousel-control hidden-xs"
				href="#main-carousel" data-slide="next"><i
				class="fa fa-angle-right"></i></a>
		</div>
	</section>
	<!--/#home-->

	<!-- -------------------------------------------------------------------------------------------------------------------- -->
	<section id="services" class="parallax-section">
		<div class="container">
			<div class="row text-center">
				<div class="col-sm-8 col-sm-offset-2">
					<h2 class="title-one">Services</h2>
					<p>Here we have listed the list of services that are provided
						by this framework; SauceLabs- a third party cloud service provider
						whcih allows to run the scripts in different browser; Jenkins-
						CI/CD tool allows you to schedule your jobs; Reports-See all your
						test reports here</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="our-service">
						<div class="services row">
							<div class="col-sm-4">
								<div class="single-service">
									<a href="http://saucelabs.com/" target="_blank"> <i
										class="fa fa-th"></i></a>
									<h2>SauceLabs</h2>
									<p>See all test cases running in cloud based environment
										called saucelabs with various verions of Browser and with
										various devices</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="single-service">
								<a href="http://localhost:8082" target="_blank">
									<i class="fa fa-html5"></i>
									<h2>Jenkins dashboard</h2></a>
									<p>See all Jobs configured in continous integration tool
										called jenkins and run your jobs and schedule your jobs as
										Daily run or weekly run</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="single-service">
									<a href="./ATU Reports/index.html" target="_blank"> <i
										class="fa fa-users"></i></a>
									<h2>Reports</h2>
									<p>See all previous reports in buildwise manner, ATU
										Reports are used for Reporting which provides you GUI Based
										reports with screenshots error</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#service-->
	<!-- ---------------------------------------------------------------------------------------------------------------- -->
	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
		ArrayList<String> Testcase = ExcelReader.ReadDriverSuiteExcel("Regression");
	%>
	<section id="regression">
		<div class="container">
			<div class="row text-center clearfix">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="contact-heading">
						<h2 class="title-one">Regression Suite</h2>
						<p>Regression Testing is defined as a type of software testing
							to confirm that a recent program or code change has not adversely
							affected existing features.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="services row">
						<div id="contact-form-section">
							<!-- 							<div class="status alert alert-success" style="display: none"></div> -->
							<form action="regression" id="contact-form" class="contact"
								name="contact-form">
								<div class="form-group">
								
									<h3>Select TestCases To Run: </h3>
									
									<table>
									<tr><td><h4><input type=checkbox onclick='selectAllReg()' value="Select All"/>Select All</h4></td></tr>
			
										<%
											for (int i = 0; i < Testcase.size(); i++) {
												//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
												out.println("<tr><td><h4><input type='checkbox' name='RegTestCases' value='" + Testcase.get(i) + "'>"
														+ "  " + Testcase.get(i) + "</h4></td></tr>");
											}
										%>

										<tr>
											<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
												<button type="cancel" class="btn btn-primary" name='Cancel'>Cancel
													Test</button></td>
										</tr>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Regression-->
	<!-- -------------------------------------------------------------------------------------------------------- -->
		<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
	ArrayList<String> Testcase1 = ExcelReader.ReadDriverSuiteExcel("Smoke");
	%>
	<section id="smoke" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Smoke Test</h2>
						<p>Smoke Testing, also known as "Build Verification Testing",
							is a type of software testing that comprises of a non-exhaustive
							set of tests that aim at ensuring that the most important
							functions work. The results of this testing is used to decide if
							a build is stable enough to proceed with further testing.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="our-service">
					<div class="services row">
						<div id="contact-form-section">
							<div class="status alert alert-success" style="display: none"></div>
							<form action="smoke" id="contact-form" class="contact"
								name="contact-form">
								<div class="form-group">
								
							<h3>Select TestCases To Run: </h3>
									
									<table>
									<tr><td><h4><input type=checkbox onclick='selectAllSmoke()' value="Select All"/>Select All</h4></td></tr>
			
										<%
											for (int i = 0; i < Testcase1.size(); i++) {
												//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
												out.println("<tr><td><h4><input type='checkbox' name='SmokeTestCases' value='" + Testcase1.get(i) + "'>"
														+ "  " + Testcase1.get(i) + "</h4></td></tr>");
											}
										%>
										
											<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
											<button type="cancel" class="btn btn-primary">Cancel
													Test</button></td>
										</tr>

									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#Smoke-->

	
	
	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
	ArrayList<String> Testcase2 = ExcelReader.ReadDriverSuiteExcel("RestAPI");
	%>
	
	<section id="webServices" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Web Services Test</h2>
						<p>Web Services Testing is testing of Web services and its
							Protocols like SOAP & REST. To test a Webservice you can Test
							ManuallyCreate your own Automation Code Use an off-the shelf
							automation tool like SoapUI.</p>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="our-service">
					<div class="services row">
						<div id="contact-form-section">
							<div class="status alert alert-success" style="display: none"></div>
							<form action="rest" id="contact-form" class="contact"
								name="contact-form">
								<div class="form-group">
									<h3>Select the testcases to run</h3>
									<table>
									<tr><td><h4><input type=checkbox onclick='selectAllRest()' value="Select All"/>Select All</h4></td></tr>
			
										<%
											for (int i = 0; i < Testcase2.size(); i++) {
												
												out.println("<tr><td><h4><input type='checkbox' name='RestAPITests' value='" + Testcase2.get(i) + "'>"
														+ "  " + Testcase2.get(i) + "</h4></td></tr>");
											}
										%>
										<div class="form-group">
											<tr>
												<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
												<button type="cancel" class="btn btn-primary">Cancel
														Test</button></td>
											</tr>
										</div>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
	ArrayList<String> Testcase3 = ExcelReader.ReadDriverSuiteExcel("SauceLabs");
	%>
	
	<section id="saucelabs" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">Sauce Labs</h2>
						<p>See all test cases running in cloud based environment called Sauce Labs with various versions of Browser and with various devices</p>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="our-service">
					<div class="services row">
						<div id="contact-form-section">
							<div class="status alert alert-success" style="display: none"></div>
							<form action="saucelabs" id="contact-form" class="contact"
								name="contact-form">
								<div class="form-group">
									<h3>Select the testcases to run</h3>
									<table>
									<tr><td><h4><input type=checkbox onclick='selectAllSauce()' value="Select All"/>Select All</h4></td></tr>
			
										<%
											for (int i = 0; i < Testcase3.size(); i++) {
												
												out.println("<tr><td><h4><input type='checkbox' name='SaucelabTests' value='" + Testcase3.get(i) + "'>"
														+ "  " + Testcase3.get(i) + "</h4></td></tr>");
											}
										%>
										<div class="form-group">
											<tr>
												<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
												<button type="cancel" class="btn btn-primary">Cancel
														Test</button></td>
											</tr>
										</div>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	<%@ page import="java.util.ArrayList"%>
	<%@ page import="utilities.ExcelReader"%>

	<%
	ArrayList<String> Testcase4 = ExcelReader.ReadDriverSuiteExcel("SeleniumGrid");
	%>
	
	<section id="seleniumgrid" class="parallax-section">
		<div class="container">
			<div class="clients-wrapper">
				<div class="row text-center">
					<div class="col-sm-8 col-sm-offset-2">
						<h2 class="title-one">SeleniumGrid</h2>
						<p>Selenium Grid is a part of the Selenium Suite that specializes in running multiple tests across different browsers, operating systems, and machines in parallel.</p>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="our-service">
					<div class="services row">
						<div id="contact-form-section">
							<div class="status alert alert-success" style="display: none"></div>
							<form action="seleniumgrid" id="contact-form" class="contact"
								name="contact-form">
								<div class="form-group">
									<h3>Select the testcases to run</h3>
									<table>
									<tr><td><h4><input type=checkbox onclick='selectAllGrid()' value="Select All"/>Select All</h4></td></tr>
			
										<%
											for (int i = 0; i < Testcase4.size(); i++) {
												
												out.println("<tr><td><h4><input type='checkbox' name='GridTests' value='" + Testcase4.get(i) + "'>"
														+ "  " + Testcase4.get(i) + "</h4></td></tr>");
											}
										%>
										<div class="form-group">
											<tr>
												<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
												<button type="cancel" class="btn btn-primary">Cancel
														Test</button></td>
											</tr>
										</div>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	
	<!-- -------------------------------------------------------------------------------------------------------- -->
	 <section id="uiVerification">
		<div class="container">
			<div class="row text-center clearfix">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="contact-heading">
						<h2 class="title-one">UI Verification</h2>
						<p>When we're talking about visual testing we want to make
							sure that the UI itself looks right to the user and that each UI
							element appears in the right color, shape, position and size. We
							also want to ensure that it doesn't hide or overlap any other UI
							elements.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="rows">
			<div id="contact-form-section">
				<div class="status alert alert-success" style="display: none"></div>
				<form action="regression" id="contact-form" class="contact"
					name="contact-form">
					<div class="form-group">
						<h3>Select the testcases to run</h3>
						<table>
							<tr>
								<td><h4>
										</td>
							</tr>
							<tr>
								<td><button type="submit" class="btn btn-primary"
													name='submit'
													onClick="$('#LockOn').css('visibility', 'visible');">Run
													Test</button>
								<button type="cancel" class="btn btn-primary">Cancel
										Test</button></td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</div>


	</section> 
	<!--/#portfolio-->
	<!-- --------------------------------------------------------------------------------------------------------------------- -->
	<!-- ------------------------------------------------------------------------------------------------------ -->
	<footer id="footer">
		<div class="container">
			<div class="text-center">
				<p>
					Copyright &copy; 2017 - <a href="https://www.mercer.com/">Mercer</a>
					| All Rights Reserved
				</p>
			</div>
		</div>
	</footer>
	<!--/#footer-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/jquery.parallax.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>